//! 1) Création du product router
const productRouter = require("express").Router();

// Import de notre controlleur, pour pouvoir utiliser les méthodes de ce dernier
const productController = require("../controllers/product.controller");

// Import du middleware de route
const existingProductMiddleware = require("../middlewares/existingProduct.middleware");

//! 2) Setup des routes de product router
//productRouter.method
    // get (récupérer des données)
    // post (envoyer des données)
productRouter.get('/', productController.getAll)
productRouter.get('/fruits', productController.getFruits)
productRouter.get('/:name([a-zA-Z]+)', existingProductMiddleware, productController.getProduct)

//! Attention, je ne peux pas avoir deux fois 
//!   /products/segmentdynamique
//! Il n'y a que le premier qui déclaré dans les routes qui sera pris en compte

// productRouter.get('/:category', (req, res) => {
productRouter.get('/category/:category', productController.getByCategory)

//! 3) Export du product router
module.exports = productRouter;