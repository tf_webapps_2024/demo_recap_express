const productController = {
    getAll : (req, res) => {
        if(!req.query.category) {
            res.send('<h1>Tous les produits (aucun filtre selectionné)</h1>')
        }
        else {
            res.send(`<h1>Tous les produits triés avec la query ${JSON.stringify(req.query)}</h1>`)
        }
    },

    getFruits : (req, res) => {
        // res.send(`
        // <h1> Affichage des fruits  </h1>
        // `)
        throw Error('Impossibe d\'accéder aux fruits');
        
    },

    getProduct : (req, res) => {
        //"/:name" -> segment dynamique 
        //Quand je vais taper localhost:8080/products/banane :
        // localhost:8080/products -> partie statique
        // /banane -> partie dynamique : Je peux mettre le nom que je veux ici, il se retrouvera stocké dans mon paramètre name
    
        //Pour récupérer la valeur qui a été tapée dans le segment dynamique, il se trouve dans req.params.nomSegment
        // req.params est un objet avec tous les paramètres (segment dyn) de la route
        res.send(`
        <h1> Affichage du produit ${req.params.name} </h1>
        `)
    },

    getByCategory : (req, res) => {
        res.send(`
            <h1> Affichage des produits de la catégorie  </h1>
    
        `)
    }
}

module.exports = productController;