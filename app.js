//! 1) Importer express :
const express = require("express");


//! 2) Créer le serveur :
const app = express();

// ? Utilisation d'application middleware
const loggerMiddleware = require("./middlewares/logger.middleware");
app.use(loggerMiddleware);
//Pour toute l'app (donc chacune des requêtes, on va passer par notre middleware)

// ? Utilisation des routes
//#region explications router

// Plutôt qu'ajouter toutes les routes dans le app.js, on va plutôt, créer un fichier de routing...
// app.get('/pouet', () => {})
// app.get('/cacahuete', () => {})

// ..et indiquer à notre app, qu'elle doit l'utiliser
// Quand on met juste le nom de dossier dans le require, il va chercher un fichier qui s'appelle index.js c'est pour ça que notre router de base est dans un fichier index.js
// Si mon routeur de base avait été dans un fichier appelé router.js, j'aurais du l'importer comme suit :
// const router = require("./routes/router");

//#endregion explications router
const router = require("./routes");
app.use(router);

// ? Utilisation du middleware error : Toujours après toute la config de l'app mais avant listen
const errorMiddleware = require("./middlewares/error.middleware");
app.use(errorMiddleware)

//! 3) Le lancer sur un port :
// process.env -> Variables d'environnement
// On aura besoin d'indiquer lors du lancement de notre application (npm start) le fichier .env qu'il devra aller mettre dans les variables d'environnement
app.listen(process.env.PORT , () => {
    console.log(`[STARTED] Server on port ${process.env.PORT}`);
})