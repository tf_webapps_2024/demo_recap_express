//Router middleware : Applicable sur une(des) route(s) en particulier

const { request, response } = require("express")
/**
 * @param {request} req
 * @param {response} res
 */

const existingProducts = ['banane', 'pêche', 'pomme', 'patate', 'melon', 'tomate'];

const existingProductMiddleware = (req, res, next) => {

    //si le param 'name' est un des fruits dans la liste : on continue la requête, sinon, on met fin à la requête avec un message d'erreur
    if( existingProducts.includes( req.params.name ) ){
        next();
    }
    else {
        res.send("<h1>Ce produit n'est pas en stock</h1>");
    }

}

module.exports = existingProductMiddleware;