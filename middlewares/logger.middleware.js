// Application level middleware, qui va intercepter chacune des requêtes et écrire Hello + votre nom + heure + infos sur la requête
// Un middleware est une fonction qui va recevoir la requête (req, res) et un callback next, pour passer à la suite de la requête

const { request, response } = require("express");

/** 
 * Js doc pour avoir l'autocomplétion sur req et res
 * @param {request} req
 * @param {response} res
 */
const loggerMiddleware = (req, res, next) => {
    
    console.log(`
    [${new Date().toLocaleTimeString()}] Hello Aude
    [Infos Requête] :
    [Url] : ${req.url}
    [Method] : ${req.method}
    [Query] : ${JSON.stringify(req.query)}
    [Body] : ${req.body}\n
    `);

    next(); //Pour indiquer qu'on continue la requête
}

module.exports = loggerMiddleware;